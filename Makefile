DECLENSIONS := $(shell find declensions -name "*.tex")

svarðfjønr.pdf: svarðfjønr.tex $(DECLENSIONS)
	xelatex svarðfjønr.tex

.PHONY: clean
clean:
	rm svarðfjønr.aux
	rm svarðfjønr.log
	rm svarðfjønr.pdf
